#include <TheThingsNetwork.h>
#include <TvBLoRaWANNodeRev2.h>
TvBLoRaWANNodeRev2 TvBLoRaWANNodeRev2;
#include <avr/sleep.h>
#include <avr/power.h>

#define debugSerial Serial
#define loraSerial Serial1


const int butPin = 2;      // pin used for waking up, same as button but here we use the int.2 from the arduino
const int ledPin = 14;      // Pin for LED
const int PIN_SERIAL1_TX = 10;
bool sleepCmd;

TheThingsNetwork ttn(loraSerial, debugSerial, TTN_FP_EU868);

void setup(){    
    debugSerial.begin(115200);
    loraSerial.begin(57600);
    while (!debugSerial && millis() < 10000);
    TvBLoRaWANNodeRev2.Init();
    TvBLoRaWANNodeRev2.RN2483_Init();
    TvBLoRaWANNodeRev2.RN2483_Reset();
    

    pinMode(butPin, INPUT_PULLUP);
    pinMode(ledPin,OUTPUT);      // set pin 13 as an output so we can use LED to monitor
    digitalWrite(ledPin,LOW);   // turn pin 13 LED on
    attachInterrupt(butPin, pinInterrupt, FALLING);
    delay(10);
    ttn.showStatus(); 
}

void loop(){
    // Stay awake for 1 second, then sleep.
    // LED turns off when sleeping, then back on upon wake.
    debugSerial.println(digitalRead(butPin));
    digitalWrite(ledPin, LOW);
    delay(50);
    if(digitalRead(butPin) == HIGH){
      delay(1000);
      sleepNow();
    }
}
                //
void sleepNow(void){
    ttn.showStatus();
    attachInterrupt(butPin, pinInterrupt, FALLING); //not working at the moment
    delay(1000);
    // Choose our preferred sleep mode:
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);

    // Set SE bit
    sleep_enable();

    // Set RN 2483 to sleep
    radioSleep(true);
    delay(500);

    // disable power consuming stuff
    power_adc_disable();
    power_spi_disable();
    power_timer1_disable(); 
    power_timer0_disable(); 
    power_twi_disable();
    
    
    digitalWrite(ledPin,HIGH);   // turn LED off to indicate sleep
    debugSerial.println("going to sleep");
    //delay(100);
    sleep_mode(); // This is where we sleep
    
    sleep_disable(); //this is where we wake up!
    
    //wake up everything
    power_adc_enable();
    power_spi_enable();
    power_timer1_enable(); 
    power_timer0_enable(); 
    power_twi_enable();
    
    delay(10);
    //detachInterrupt(2);
    debugSerial.println("interrupt detached");
    delay(10);

    // Wake up RN2483
    radioSleep(false);
    delay(50);
    ttn.showStatus();
    
}

void pinInterrupt(){
  debugSerial.println("waking");
}


void radioSleep(bool sleepCmd){
  if(sleepCmd == true){
      String str = "sys sleep 4000000000";
      loraSerial.println(str);
      loraSerial.flush();
      return;
  }
  if(sleepCmd == false){
    debugSerial.println("hello");
    loraSerial.flush();
    delay(50);
    loraSerial.end();
    delay(50);
    loraSerial.begin(57600);
    delay(50);
    loraSerial.write((uint8_t)0x00);
    loraSerial.flush();
    loraSerial.write((uint8_t)0x55);
    loraSerial.flush();
    debugSerial.println("bye");
    return;
  }
}
